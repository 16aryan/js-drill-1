// ==== Problem #3 ====
// The marketing team wants the car models listed alphabetically on the website. Execute a function to Sort all the car model names into alphabetical order and log the results in the console as it was returned.
const inventory = require('../inventory');

function problem3(inventory){
    let carData = [];
    if(!inventory || typeof inventory!= 'object'){
        return console.log('please provide inventory');
    }
    for(let data =0;data<inventory.length;data++){
        carData[data]=(inventory[data].car_model);
    }
    for(let data =0;data<carData.length;data++){
        for(let name=data;name<carData.length;name++){
            if(carData[data]>carData[name]){
            let temp = carData[data];
            carData[data]=carData[name];
            carData[name]=temp;
            }
        }
    }
    console.log(carData);
}

problem3(inventory);
module.exports = problem3;