
// ==== Problem #6 ====
// A buyer is interested in seeing only BMW and Audi cars within the inventory.  Execute a function and return an array that only contains BMW and Audi cars.  Once you have the BMWAndAudi array, use JSON.stringify() to show the results of the array in the consol
const inventory = require('../inventory');

function problem6(inventory){
    let carNames = [];
    if(!inventory || typeof inventory!= 'object'){
      return console.log('please provide inventory');
  }
    for(let data=0;data<inventory.length;data++){
        if(inventory[data].car_make== 'Audi' || inventory[data].car_make == 'BMW'){
          let desiredCar = inventory[data];
          carNames.push(desiredCar);
        }

    }
 console.log(JSON.stringify(carNames)); 
}

//getCarName(inventory);

module.exports = problem6;