
// ==== Problem #4 ====
// The accounting team needs all the years from every car on the lot. Execute a function that will return an array from the dealer data containing only the car years and log the result in the console as it was returned.



const inventory =require('../inventory');


function problem4(inventory){
    let carYears =[];
    if(!inventory || typeof inventory!= 'object'){
        return console.log('please provide inventory');
    }
    for(let data =0;data<inventory.length;data++){
        carYears[data]=inventory[data].car_year;
    }
 //   console.log(carYears);
 return carYears;
}
problem4(inventory);
module.exports = problem4;